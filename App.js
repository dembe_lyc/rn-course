import React from 'react';
import {StyleSheet, Text, View, TextInput, Button} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {addPlace, deletePlace, selectPlace, unselectPlace} from './src/store/actions/places';

import ListItems from './src/components/listItems/listItems';
import PlaceInput from './src/components/placeInput/placeInput';
import PlaceDetails from './src/components/placeDetail/placeDetail';

import ColeImage from './src/assets/j-cole-kingsize.no_.jpg'

class App extends React.Component {

  state = {
    placeName: '',
    places: [],
    selectedPlace: null
  };

  onAddPlaces = (place) => {

    this.props.onAddPlaces(place);
  };

  onItemDelete = () => {

    this.props.onItemDelete();
  }

  onItemSelect = (key) => {
    
    this.props.onItemSelect(key);

  }

  onCloseModal = () => {

    this.props.onCloseModal();
  }

  render() {
   
    return (
      <View style={styles.container}>
        <PlaceDetails selectedPlace={this.props.selectedPlace} onCloseModal={this.onCloseModal}
           onItemDelete={this.onItemDelete} />
        <PlaceInput onAddPlaces={this.onAddPlaces}/>
        <ListItems places={this.props.places} onItemSelect={this.onItemSelect}/>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start'
  }
});

const mapStateToProps = (state) => {

  return {
    places: state.places.places,
    selectedPlace: state.places.selectedPlace
  }
}

const mapDispatchToProps = (dispatch) => {

  return bindActionCreators({
    onAddPlaces: addPlace,
    onCloseModal: unselectPlace,
    onItemDelete: deletePlace,
    onItemSelect: selectPlace
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
