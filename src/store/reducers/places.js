import {ADD_PLACE, DELETE_PLACE, SELECT_PLACE, UNSELECT_PLACE} from '../actions/actionTypes';

const initState = {
    placeName: '',
    places: [],
    selectedPlace: null
}

const PlacesReducer = (state=initState, action) => {
    switch (action.type) {
        case ADD_PLACE:

            return {
                ...state,
                places: state.places.concat({
                    name: action.placeName,
                    key: Math.random(),
                    image: {
                      uri: 'http://kingsize.no/wp-content/uploads/2016/12/j-cole-kingsize.no_.jpg'
                    }
                  })
            }
        break;
        case DELETE_PLACE:

            return {
                ...state,
                places: state.places.filter((place) => {
                    return place.key !== state.selectedPlace.key
                }),
                selectedPlace: null
            }
        break;
        case SELECT_PLACE:

            return {
                ...state,
                selectedPlace: state.places.find((place) => {
                    return place.key === action.placeKey
                })
            }
        break;
        case UNSELECT_PLACE:

            return {
                ...state,
                selectedPlace: null
            }
        break;
        default:
            return state;
    }
}

export default PlacesReducer;