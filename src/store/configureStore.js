import {createStore, combineReducers, compose} from 'redux';

import PlacesReducer from './reducers/places';

const rootReducer = combineReducers({
    places: PlacesReducer
});

let composeEnhencers = compose;

if(__DEV__){
    composeEnhencers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const combinedReducers = () => {
    return createStore(rootReducer, composeEnhencers());
}

export default combinedReducers;