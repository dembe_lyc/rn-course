import React from 'react';
import {View, Text, Button, Modal, Image, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

let PlaceDetails = (props) => {

    let imageText = null;

    if(props.selectedPlace){
        imageText = (
            <View>
                <Image source={props.selectedPlace.image} style={styles.image}/>
                <Text style={styles.text}>{props.selectedPlace.name}</Text>
            </View> 
        );
    }

    deleteItem = () => {
        props.onItemDelete(props.selectedPlace.key)
    }

    return (
        <View>
            <Modal onRequestClose={props.onCloseModal} visible={props.selectedPlace !== null}>
                {imageText}
                <View>
                    <TouchableOpacity onPress={this.deleteItem} style={styles.trash}>
                        <Icon size={30} color="red" name="ios-trash"/>
                    </TouchableOpacity>
                    <Button title="Close" onPress={props.onCloseModal} />
                </View>
            </Modal>
        </View>
    );
}

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 200
    },
    text: {
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 20,
    },
    trash: {
        alignItems: 'center'
    }
});

export default PlaceDetails;