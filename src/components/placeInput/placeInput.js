import React from 'react';
import {View, TextInput, Button, StyleSheet} from 'react-native';


export default class PlaceInput extends React.Component {

  state = {
    placeName: ''
  };

  inputHandler = (placeName) => {

    this.setState({placeName})
  };

  addPlaces = () => {

    if(this.state.placeName.trim() === ""){
      return;
    }

    this.props.onAddPlaces(this.state.placeName);
    //this.setState({placeName: ''})

  };

  render(){

    return(
      <View style={styles.inputButtonContainer}>
        <TextInput value={this.state.placeName} onChangeText={this.inputHandler}
          style={styles.inputPlacer} placeholder="Name of an awsome place"/>
        <Button title="Add" style={styles.buttonPlacer} onPress={this.addPlaces}/>
      </View>
    )
  }
}


const styles = StyleSheet.create({

  inputButtonContainer: {
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  inputPlacer:{
    width: "70%"
  },
  buttonPlacer: {
    width: "30%"
  }
})
