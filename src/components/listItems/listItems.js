import React from 'react';
import {FlatList, Text, StyleSheet} from 'react-native';

import SingleListItem from '../singleListItem/singleListItem';

let ListItems = (props) => {

  //alert(JSON.stringify(props.places))

  return(
    <FlatList contentContainerStyle={styles.container} style={styles.width}
      data = {props.places}
      renderItem = {(info) => (
        <SingleListItem place={info.item.name} 
        image={info.item.image} onItemSelect={() => {props.onItemSelect(info.item.key)}}/>
      )}
      />
  )
}

const styles = StyleSheet.create({

  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
  width: {
    width: '100%'
  }
});

export default ListItems;
