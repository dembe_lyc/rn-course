import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

let SingleListItem = (props) => {

  return (
    <TouchableOpacity onPress={props.onItemSelect}>
      <View style={styles.textContainer}>
        <Image source={props.image} style={styles.imageContainer}/>
        <Text>{props.place}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  textContainer: {
    backgroundColor: "#eee",
    marginBottom: '3%',
    marginLeft: '5%',
    marginRight: '5%',
    flexDirection: 'row',
    alignItems: 'center',
    
  },
  imageContainer: {
    marginRight: 10,
    width: 30,
    height: 30
  }
})

export default SingleListItem;
