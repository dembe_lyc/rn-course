import React from 'react';
import { AppRegistry } from 'react-native';
import {Provider} from 'react-redux'

import App from './App';

import combinedReducers from './src/store/configureStore';

const store = combinedReducers();

let RRDX = () => (
    <Provider store={store}>
        <App/>
    </Provider>
)

AppRegistry.registerComponent('rncourse', () => RRDX);
